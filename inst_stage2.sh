#! /bin/bash

sudo reflector --country Poland --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

packs_pacman=(
    sway
    swaylock
    swayidle
    nautilus
    firefox
    unzip
    neofetch
    kitty
    ranger
    ueberzug
    zsh-autosuggestions
    zsh-syntax-highlighting
)

sudo pacman -S ${packs_pacman[@]}

git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si --noconfirm
yay -Y --gendb && yay -Syu --devel

packs_yay=(
    zramd
    vieb-bin
    vim-plug
    zsh-vi-mode-git
    dmenu-wayland-git
    zsh-theme-powerlevel10k-git
)

yay -S ${packs_yay[@]}

