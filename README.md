Arch btrfs

1. Odśwież repozytoria
	reflector --country Poland --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
	pacman -Sy

2. Sprawdź dyski
	lsblk

3. Stwórz partycje
	gdisk /dev/sda

	GRUB:
    - +1M
    - ef02
	ROOT:
    - reszta
    - 8300

4. Zamontuj partycje:
	GRUB:
    - mkfs.vfat /dev/sda1
	ROOT:
    - mkfs.btrfs /dev/sda3
    - mount /dev/sda3 /mnt
    - cd /mnt
    - btrfs subvolume create @
    - btrfs subvolume create @home
    - btrfs subvolume create @var
    - cd
    - umount /mnt
    - mount -o noatime,compress=lzo,space_cache=v2,discard=async,subvol=@ /dev/sda2 /mnt
    - mkdir /mnt/{home,var}
    - mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/sda2 /mnt/home
    - mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var /dev/sda2 /mnt/var

5. Zainstaluj podstawowe pakiety:
pacstrap /mnt 
    - base 
    - linux
    - linux-firmware
    - git
    - neovim
    - zsh
    - intel-ucode
    - btrfs-progs 
6. Zapisz układ dysku:
    - genfstab -U /mnt >> /mnt/etc/fstab

7. Zmień użytkownika:
	arch-chroot /mnt

8. Użyj skryptu instalacyjnego
	https://gitlab.com/xprob/arch_install  	https://gitlab.com/eflinux/arch-basic

8. GRUB:
	vim /etc/default/grub
	GRUB_GFXMODE=1920x1080
	GRUB_CMDLINE_LINUX_DEFAULT="video=1920x1080"

	grub-mkconfig -o /boot/gru	b/grub.cfg

9. Odmontuj archa i restartuj
	unmount -a 
	reboot

Yay:
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si
	yay -Y --gendb
	yay -Syu --devel

Pakiety YAY:
	zramd
	vieb
	dmenu-wayland-git
	vim-plug
	zsh-vi-mode-git

SWAY:
	mkdir -p ~/.config/sway
	cp /etc/sway/config ~/.config/sway

ZRAM:
	vim /etc/default/zramd
	MAX_SIZE=2048
	sudo systemctl enable —now zramd.service

Czcionka:
	mkdir -p /usr/local/share/fonts/ttf
	fc-cache
	https://www.nerdfonts.com

Doty:
	https://gitlab.com/xprob/dot_files 

Python:
	Miniforge3-Linux-x86_64

