#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
sed -i '390s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=pl" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script

packs=(
    grub
    grub-btrfs
    networkmanager
    network-manager-applet
    dialog
    iwd
    mtools
    dosfstools
    reflector
    base-devel
    linux-headers
    avahi
    tlp
    xdg-desktop-portal-impl
    xdg-user-dirs
    xdg-utils
    gvfs
    gvfs-smb
    nfs-utils
    inetutils
    dnsutils
    bluez
    bluez-utils
    cups
    hplip
    alsa-utils
    pipewire
    pipewire-alsa
    pipewire-pulse
    pipewire-jack
    openssh
    rsync
    reflector
    acpi
    acpi_call
    virt-manager
    qemu
    qemu-arch-extra
    edk2-ovmf
    bridge-utils
    dnsmasq
    vde2
    openbsd-netcat
    ipset
    firewalld
    sof-firmware
    nss-mdns
    acpid
    os-prober
    ntfs-3g
    ttf-jetbrains-mono
)

pacman -S ${packs[@]}

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=i386-pc /dev/sda # replace sdx with your disk name, not the partition
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable tlp
systemctl enable iwd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m barc
echo barc:1234 | chpasswd
usermod -aG libvirt barc

echo "barc ALL=(ALL) ALL" >> /etc/sudoers.d/barc

chsh -s /bin/zsh barc

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m\n"

